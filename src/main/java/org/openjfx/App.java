package org.openjfx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {

        mostrarEjercicio(stage, this.elegirEjercicios());
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }


    public static void main(String[] args) {
        launch();
    }


    public void mostrarEjercicio(Stage stage, String nombre) throws IOException {

        scene = new Scene(loadFXML(nombre));
        stage.setScene(scene);
        stage.setTitle(nombre);
        stage.show();

    }

    public String elegirEjercicios() {
        String ejer = "";
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Selecciona una opción");
        alert.setHeaderText("Seleccione ejercicio para abrir");
        alert.setContentText("");

        ButtonType bt1 = new ButtonType("Calculadora");
        ButtonType bt2 = new ButtonType("Formulari");
        ButtonType bt3 = new ButtonType("Lista");
        ButtonType bt4 = new ButtonType("TablaView");

        alert.getButtonTypes().setAll(bt1, bt2, bt3, bt4);

        Optional<ButtonType> result = alert.showAndWait();


        switch (result.get().getText()) {
            case "Calculadora":

                ejer = "calculadora";
                break;
            case "Formulari":
                ejer = "formulari";
                break;
            case "Lista":

                ejer = "list";
                break;
            default:
                ejer = "tablaView";
        }
        return ejer;

    }


}