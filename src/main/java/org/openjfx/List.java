package org.openjfx;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ListChangeListener;
import javafx.collections.FXCollections;
import javafx.scene.control.*;

public class List implements Initializable {
    @FXML
    private TextField tfañadir;

    @FXML
    private TextField tfborrar;

    @FXML
    private TextField seleccionado;

    @FXML
    private ListView<String> seleccionarVarios;


   private  ObservableList<String> observableList;
    private ObservableList<String> selectedItems;
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        ArrayList<String> lista = new ArrayList<String>();
        lista.add("Madre");
        lista.add("Padre");
        lista.add("Hijo");
        lista.add("Hija");
        lista.add("Primo");

       observableList = FXCollections.observableList(lista);

        observableList.addListener(new ListChangeListener() {

            @Override
            public void onChanged(ListChangeListener.Change change) {

            }
        });
        seleccionarVarios.setItems(observableList);
        //activamos que sea seleccion multiple
        seleccionarVarios.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);



        seleccionarVarios.setOnMouseClicked(new EventHandler<Event>() {


            public void handle(Event event) {
                //cogemos todos los seleccionados
                selectedItems =  seleccionarVarios.getSelectionModel().getSelectedItems();

                seleccionado.setText(selectedItems.get(0));
                

            }

        });

    }


    @FXML
    void añadir(ActionEvent event) {
        if(!tfañadir.getText().equals(""))
            observableList.add(tfañadir.getText());

    }

    @FXML
    void borrar(ActionEvent event) {
        //comprobamos que el valor no este vacio
       if(!tfborrar.getText().equals("")){
           //comprobacion que es un numero y que no sea mayor que el index
           if(isNumeric(tfborrar.getText())&& Integer.parseInt(tfborrar.getText())<observableList.size()){

               observableList.remove(Integer.parseInt(tfborrar.getText()));
           }
       }

    }
    //para comprobar que es numero entero
    private static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }
  
    @FXML
    void allSelect(ActionEvent event) {
        String selecionados="";
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Elementos Seleccionados");
        alert.setTitle("Datos");
        for(String s : selectedItems){
           selecionados+=s+"\n";
        }

        alert.setContentText(selecionados);

        alert.showAndWait();

    }
}
