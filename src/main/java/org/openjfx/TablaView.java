package org.openjfx;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;


import java.net.URL;
import java.util.ResourceBundle;

public class TablaView implements Initializable {

    @FXML
    private TableView<Producto> tvContenido;

    @FXML
    private TableColumn<Producto, String> colNombre;

    @FXML
    private TableColumn<Producto, String> colDimensiones;

    @FXML
    private TableColumn<Producto, Double> colPeso;

    @FXML
    private TextField tfnombre;

    @FXML
    private TextField tfDimensiones;

    @FXML
    private TextField tfPeso;


    @FXML
    void btInsertar(ActionEvent event) {


        ObservableList<Producto> productos = FXCollections.observableArrayList(new Producto(tfnombre.getText(), tfDimensiones.getText(), Double.parseDouble(tfPeso.getText())));
        //Metemos los datos a la tabla
        tvContenido.setItems(productos);

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        //Asociamos la columna con los datos que vamos a introducir
        colNombre.setCellValueFactory(new PropertyValueFactory<Producto, String>("Nombre"));
        colDimensiones.setCellValueFactory(new PropertyValueFactory<Producto, String>("Dimensiones"));
        colPeso.setCellValueFactory(new PropertyValueFactory<Producto, Double>("Peso"));

    }


}
