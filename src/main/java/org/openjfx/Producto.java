package org.openjfx;

public class Producto {
    private String nombre;
    private String dimensiones;
    private double peso;

    public Producto(String nombre,String dimensiones, double peso) {
        this.nombre = nombre;
        this.dimensiones= dimensiones;
        this.peso = peso;
    }

    public String getDimensiones() {
        return dimensiones;
    }

    public void setDimensiones(String dimensiones) {
        this.dimensiones = dimensiones;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }
}
