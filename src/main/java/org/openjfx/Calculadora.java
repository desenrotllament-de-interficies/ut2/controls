package org.openjfx;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ResourceBundle;

public class Calculadora implements Initializable {
    private double operador1;
    private double operador2;
    private double resultado;

    @FXML
    private TextField oper1;

    @FXML
    private TextField oper2;

    @FXML
    private TextField result;

    @FXML
    private Button btnCalc;

    @FXML
    private RadioButton sumar;

    @FXML
    private RadioButton restar;

    @FXML
    private RadioButton multiplicar;

    @FXML
    private RadioButton dividir;

    @FXML
    private ToggleGroup grupoDeOperadores;

    @FXML
    void calcular(ActionEvent event) {

        operaciones();
    }

    @FXML
    void dividir(ActionEvent event) {

    }

    @FXML
    void multiplicar(ActionEvent event) {

    }

    @FXML
    void restar(ActionEvent event) {

    }

    @FXML
    void sumar(ActionEvent event) {

    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    public void operaciones() {
        RadioButton rbtt;
        Alert alert = new Alert(Alert.AlertType.ERROR);
        System.out.println();

        //Comprobamos que no esten vacios los operadores
        if (oper1.getText() != "" && oper2.getText() != "") {
            //Comprobacion de que los dos operadores son numeros
            if (isNumeric(oper1.getText()) && isNumeric(oper2.getText())) {
                //pasamos el contenido de los operadores en numeros
                operador1 = Double.parseDouble(oper1.getText());
                operador2 = Double.parseDouble(oper2.getText());

                //comprobacion de que hemos seleccionado el tipo de operacion a realizar
                if (grupoDeOperadores.getSelectedToggle() != null) {
                    //cogemos la operacion seleccionado
                    rbtt = (RadioButton) grupoDeOperadores.getSelectedToggle();
                    switch (rbtt.getText()) {
                        case "Sumar":
                            resultado = operador1 + operador2;
                            result.setText(String.valueOf(resultado));

                            break;
                        case "Restar":
                            resultado = operador1 - operador2;
                            result.setText(String.valueOf(resultado));
                            break;
                        case "Multiplicar":
                            resultado = operador1 * operador2;
                            result.setText(String.valueOf(resultado));
                            break;
                        case "Dividir":
                            if (operador2 == 0) {
                                alert.setHeaderText(null);
                                alert.setTitle("Error");
                                alert.setContentText("Error, la división por cero no está definida");
                                alert.showAndWait();
                            }
                            resultado = operador1 + operador2;
                            result.setText(String.valueOf(resultado));
                            break;

                        default:
                    }
                } else {

                    alert.setHeaderText(null);
                    alert.setTitle("Error");
                    alert.setContentText("Error en la aplicacion seleccionar una Operacion");
                    alert.showAndWait();
                }


            }

        } else {
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Error introducir valores numericos en los do operadores");
            alert.showAndWait();

        }
    }

    private static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

}